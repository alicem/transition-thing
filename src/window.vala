public class ThumbnailPaintable : Object, Gdk.Paintable {
    private Gtk.Widget widget;
    private File file;
    private string tag;
    private int full_width;
    private int full_height;

    private Gdk.Texture previous_cache;
    private Gdk.Texture cache;
    private int last_width;
    private int last_height;

    private bool loading;
    private static Nostalgia.TextureLoader loader = new Nostalgia.TextureLoader ();

    public ThumbnailPaintable (File file, string tag, Gtk.Widget widget) {
        this.file = file;
        this.tag = tag.replace ("/", "-");
        this.widget = widget;

        Gdk.Pixbuf.get_file_info (file.get_path (), out full_width, out full_height);
    }

    private void snapshot (Gdk.Snapshot gdk_snapshot, double width, double height) {
        int w = (int) Math.ceil (width) * widget.scale_factor;
        int h = (int) Math.ceil (height) * widget.scale_factor;

        if ((w != last_width || h != last_height) && cache != null) {
            previous_cache = cache;
            cache = null;
        }

        if (cache == null && !loading) {
            last_width = w;
            last_height = h;
            loading = true;

            loader.load (file, w, h, tag, this, (w2, h2, t) => {
                loading = false;

                if (w2 == last_width && h2 == last_height) {
                    cache = t;
                    invalidate_contents ();
                }
            });
        }

        var snapshot = gdk_snapshot as Gtk.Snapshot;

        if (cache == null && previous_cache != null) {
            snapshot.append_texture (previous_cache, {{ 0, 0, }, { (float) width, (float) height }});
            return;
        }

        if (cache == null)
            return;

        snapshot.append_texture (cache, {{ 0, 0, }, { (float) width, (float) height }});
    }

    private int get_intrinsic_width () {
        return full_width;
    }

    private int get_intrinsic_height () {
        return full_height;
    }
}

[GtkTemplate (ui = "/org/example/App/window.ui")]
public class TransitionWidget.Window : Adw.ApplicationWindow {
    [GtkChild]
    private unowned TransitionStack stack;
    [GtkChild]
    private unowned Gtk.Widget grid_page;
    [GtkChild]
    private unowned Gtk.Widget pic_page;
    [GtkChild]
    private unowned Gtk.GridView grid;
    [GtkChild]
    private unowned Gtk.Picture pic;
    [GtkChild]
    private unowned Gtk.Label pic_title;
    [GtkChild]
    private unowned Gtk.Button pic_star_btn;

    private Gtk.StringList list;
    private Gtk.SortListModel sorted_list;
    private uint current_item;

    public Window (Gtk.Application app) {
        Object (application: app);
    }

    static construct {
        typeof (TransitionStack).ensure ();

        install_action ("window.back", null, widget => {
            var self = widget as Window;

            var obj = self.sorted_list.get_item (self.current_item) as Gtk.StringObject;
            var src_pic = obj.get_data<Gtk.Widget> ("picture");
            var src_title = obj.get_data<Gtk.Widget> ("label");
            var src_star_btn = obj.get_data<Gtk.Widget> ("star");

            self.stack.add_shared_element (self.pic, src_pic);
            self.stack.add_shared_element (self.pic_title, src_title);
            self.stack.add_shared_element (self.pic_star_btn, src_star_btn);
            self.stack.navigate (self.grid_page);

            self.action_set_enabled ("window.back", false);
        });

        add_binding_action (Gdk.Key.Left, Gdk.ModifierType.ALT_MASK, "window.back", null);
    }

    private async void add_recursively (bool count_parent, File dir) throws Error {
        var enumerator = yield dir.enumerate_children_async (
            FileAttribute.STANDARD_NAME + "," + FileAttribute.STANDARD_TYPE,
            FileQueryInfoFlags.NOFOLLOW_SYMLINKS,
            Priority.DEFAULT,
            null
        );

        FileInfo info;
        while ((info = enumerator.next_file (null)) != null) {
            if (info.get_file_type () == FileType.DIRECTORY)
                yield add_recursively (true, dir.get_child (info.get_name ()));
            else if (count_parent)
                list.append (dir.get_basename () + "/" + info.get_name ());
            else
                list.append (info.get_name ());
        }
    }

    private void fill_model () {
        add_recursively.begin (false, File.new_for_path (Config.WALLPAPERS_DIR));
    }

    private int parse_name (string name) {
        return int.parse (name.replace (".jpg", ""));
    }

    construct {
        list = new Gtk.StringList (null);
        fill_model ();

        var factory = new Gtk.SignalListItemFactory ();
        factory.setup.connect (item => {
            var grid = new Gtk.Grid ();
            item.child = grid;

            var bin = new Adw.Bin ();
            bin.width_request = 180;
            bin.height_request = 120;
            grid.attach (bin, 0, 0, 2, 1);

            var picture = new Gtk.Picture ();
            picture.halign = Gtk.Align.CENTER;
            picture.valign = Gtk.Align.CENTER;
            bin.child = picture;

            var label = new Gtk.Label (null);
            label.ellipsize = Pango.EllipsizeMode.END;
            label.xalign = 0;
            label.halign = Gtk.Align.START;
            label.add_css_class ("padded-label");
            label.hexpand = true;
            grid.attach (label, 0, 1, 1, 1);

            var star = new Gtk.Button.from_icon_name ("non-starred-symbolic");
            star.add_css_class ("flat");
            grid.attach (star, 1, 1, 1, 1);

            item.set_data<Gtk.Picture> ("picture", picture);
            item.set_data<Gtk.Label> ("label", label);
            item.set_data<Gtk.Button> ("star", star);
        });
        factory.bind.connect (item => {
            var picture = item.get_data<Gtk.Picture> ("picture");
            var label = item.get_data<Gtk.Label> ("label");
            var star = item.get_data<Gtk.Button> ("star");

            var obj = item.item as Gtk.StringObject;
            obj.set_data<Gtk.Picture?> ("picture", picture);
            obj.set_data<Gtk.Label?> ("label", label);
            obj.set_data<Gtk.Button?> ("star", star);

            var name = obj.get_string ();
            var file = File.new_build_filename (Config.WALLPAPERS_DIR, name);
            picture.paintable = new ThumbnailPaintable (file, name, picture);
            label.label = name;
        });
        factory.unbind.connect (item => {
            var obj = item.item as Gtk.StringObject;

            obj.set_data<Gtk.Widget?> ("picture", null);
            obj.set_data<Gtk.Widget?> ("label", null);
            obj.set_data<Gtk.Widget?> ("star", null);
        });

        var sorter = new Gtk.CustomSorter ((a, b) => {
            var str1 = ((Gtk.StringObject) a).get_string ();
            var str2 = ((Gtk.StringObject) b).get_string ();

            int ver1 = parse_name (str1);
            int ver2 = parse_name (str2);

            return ver1 - ver2;
        });

        sorted_list = new Gtk.SortListModel (list, sorter);
        grid.model = new Gtk.NoSelection (sorted_list);
        grid.factory = factory;

        grid.activate.connect (pos => {
            current_item = pos;

            var obj = sorted_list.get_item (pos) as Gtk.StringObject;
            var name = obj.get_string ();

            var file = File.new_build_filename (Config.WALLPAPERS_DIR, name);
            try {
                pic.paintable = Gdk.Texture.from_file (file);
            } catch (Error e) {
                critical ("Couldn't load image: %s", e.message);
            }
            pic_title.label = name;

            var src_pic = obj.get_data<Gtk.Widget> ("picture");
            var src_title = obj.get_data<Gtk.Widget> ("label");
            var src_star_btn = obj.get_data<Gtk.Widget> ("star");

            stack.add_shared_element (src_pic, pic);
            stack.add_shared_element (src_title, pic_title);
            stack.add_shared_element (src_star_btn, pic_star_btn);
            stack.navigate (pic_page);

            action_set_enabled ("window.back", true);
        });
    }
}
