/* wallpaper-row.vala
 *
 * Copyright 2020 Alexander Mikhaylenko
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Nostalgia {
    public class TextureLoader : Object {
        public delegate void TextureReadyCallback (int width, int height, Gdk.Texture? texture);

        private struct TextureRequest {
            File file;
            int width;
            int height;
            string tag;
            Object source;
            unowned TextureReadyCallback cb;
        }

        private AsyncQueue<TextureRequest?> request_queue;
        private Thread thread;

        construct {
            request_queue = new AsyncQueue<TextureRequest?> ();
            thread = new Thread<void*> (null, run_loader_thread);
        }

        private void run_callback (TextureRequest request, Gdk.Texture? texture) {
            Idle.add (() => {
                request.cb (request.width, request.height, texture);
                return Source.REMOVE;
            });
        }

        private void get_dimensions (TextureRequest request, out int width, out int height, out int x, out int y) {
            var desired_width = request.width;
            var desired_height = request.height;

            int w, h;
            Gdk.Pixbuf.get_file_info (request.file.get_path (), out w, out h);

            var aspect_ratio = (double) w / h;

            if ((double) h / desired_height > (double) w / desired_width) {
                h = (int) (desired_width / aspect_ratio);
                w = desired_width;
            } else {
                w = (int) (desired_height * aspect_ratio);
                h = desired_height;
            }

            assert (w >= desired_width);
            assert (h >= desired_height);

            width = w;
            height = h;

            x = (w - desired_width) / 2;
            y = (h - desired_height) / 2;
        }


        private void* run_loader_thread () {
            while (true) {
                var request = request_queue.pop ();
                var file = request.file;

                var texture = load_from_cache (request);
                if (texture != null) {
                    run_callback (request, texture);
                    continue;
                }

                int x, y, width, height;
                get_dimensions (request, out width, out height, out x, out y);

                try {
                    var pixbuf = new Gdk.Pixbuf.from_file_at_scale (file.get_path (), width, height, false);
                    pixbuf = new Gdk.Pixbuf.subpixbuf (pixbuf, x, y, request.width, request.height);
                    save_to_cache (request, pixbuf);

                    run_callback (request, Gdk.Texture.for_pixbuf (pixbuf));
                }
                catch (Error e) {
                    critical ("Couldn't load pixbuf for file %s: %s", file.get_path (), e.message);
                    run_callback (request, null);
                }
            }
        }

        private File get_cache_file (TextureRequest request) throws Error {
            var cache_dir = Environment.get_user_cache_dir ();
            return File.new_for_path (@"$cache_dir/transition-widget/$(request.width)x$(request.height)/$(request.tag)");
        }

        private Gdk.Texture? load_from_cache (TextureRequest request) {
            File file;
            try {
                file = get_cache_file (request);
            }
            catch (Error e) {
                critical (e.message);
                return null;
            }

            try {
                return Gdk.Texture.from_file (file);
            }
            catch (Error e) {
                return null;
            }
        }

        private void save_to_cache (TextureRequest request, Gdk.Pixbuf pixbuf) {
            try {
                var dir = get_cache_file (request);
                var parent = dir.get_parent ();

                if (!parent.query_exists ())
                    parent.make_directory_with_parents ();

                pixbuf.save (dir.get_path (), "png", null);
            }
            catch (Error e) {
                critical (e.message);
            }
        }

        public void load (File file, int width, int height, string tag, Object source, TextureReadyCallback cb) {
            request_queue.push_front ({ file, width, height, tag, source, cb });
        }
    }
}
